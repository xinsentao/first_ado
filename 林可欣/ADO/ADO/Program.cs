﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO
{
    class Program
    {
        static void Main(string[] args)
        {
            string connStr = "server=.;database=Student;uid=sa;pwd=123456";
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string sql = "select * from StudentInfo";
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}",sdr["StuId"],sdr["StuName"],sdr["StuAge"],sdr["StuSex"]);
            }
            conn.Close();
        }
    }
}
