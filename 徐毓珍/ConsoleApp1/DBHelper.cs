﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DBHelper
    {
        private static string connstr = "server=127.0.0.1;database=Users;uid=sa;pwd=123456";
        private static string sql = "select * from Userinfo";

        public static void UserinfoReader()
        {
            string connstr = "server=127.0.0.1;database=Users;uid=sa;pwd=123456";
            //获得连接
            SqlConnection conn = new SqlConnection(connstr);
            string sql = "select * from Userinfo";
            conn.Open();
            //指令对象
            SqlCommand cmd = new SqlCommand(sql, conn);
            //执行指令
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t", sdr["id"], sdr["name"], sdr["pwd"], sdr["age"], sdr["sex"]);
            }
            conn.Close();
        }
        public static void UserinfoAdapter()
        {
            DataTable dataTable = new DataTable();
            //获得连接
            SqlConnection conn = new SqlConnection(connstr);

            conn.Open();
            //适配器对象
            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);
            sda.Fill(dataTable);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t", dataTable.Rows[i]["id"], dataTable.Rows[i]["name"], dataTable.Rows[i]["pwd"], dataTable.Rows[i]["age"], dataTable.Rows[i]["sex"]);
            }
        }
    }
}
