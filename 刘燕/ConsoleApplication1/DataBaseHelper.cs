﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class DataBaseHelper
    {
        private static readonly string sqlCommandString = "select * from student";
        private static readonly string connectionString = "server=.;database=test;uid=sa;pwd=18705980290ly";

        public static void PrintUserInfoReader()
        {
            string connectionString = "server=.;database=test;uid=sa;pwd=18705980290ly";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from student";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var Sdr = sqlCommand.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t{2}", "id", "名字", "密码", "年龄");
            while (Sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", Sdr["userId"], Sdr["userName"], Sdr["userPwd"], Sdr["userAge"]);
            }

            sqlConnection.Close();

        }

        public static void PrintUserInfoAdaper()
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommandString, connectionString);

            DataTable dataTable = new DataTable();

            dataAdapter.Fill(dataTable);

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("{0}\t{1}\t{2}", "id", "名字", "密码", "年龄");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", dataTable.Rows[i]["userId"], dataTable.Rows[i]["userName"], dataTable.Rows[i]["userPwd"], dataTable.Rows[i]["userAge"]);
            }
        }
    }
}
