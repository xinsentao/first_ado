﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "server=.;database=SCHOOL;uid=sa;pwd=040520";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var SqlCommandString = "select * from class";

            SqlCommand sqlCommand = new SqlCommand(SqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}", "编号", "班级名称", "专业编号");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", sdr["classId"], sdr["className"], sdr["specialtyId"]);
            }

            sqlConnection.Close();


        }
    }
}
