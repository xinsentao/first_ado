﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ConsoleApp1
{
    class DBHelper
    {
        private static string connstr = "server=DESKTOP-512404B;database=Test;uid=sa;pwd=123456";
        private static string sql = "select * from Userinfo";

        public static void UserinfoReader()
        {
            string connstr = "server=DESKTOP-512404B;database=Test;uid=sa;pwd=123456";
            //获得连接
            SqlConnection conn = new SqlConnection(connstr);
            string sql = "select * from Users";
            conn.Open();
            //指令对象
            SqlCommand cmd = new SqlCommand(sql, conn);
            //执行指令
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t", sdr["id"], sdr["name"], sdr["sex"], sdr["age"]);
            }
            conn.Close();
        }
        public static void UserinfoAdapter()
        {
            DataTable dataTable = new DataTable();
            //获得连接
            SqlConnection conn = new SqlConnection(connstr);

            conn.Open();
            //适配器对象
            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);
            sda.Fill(dataTable);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t", dataTable.Rows[i]["id"], dataTable.Rows[i]["sex"], dataTable.Rows[i]["age"]);
            }
        }
    }
}
