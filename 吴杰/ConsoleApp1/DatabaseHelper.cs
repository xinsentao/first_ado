﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DatabaseHelper
    {
        private static string sql="select * from commodity";
        private static string constr = "server=.;database=User1;uid=sa;pwd=123456";

        public static void PrintUserInfoReader()
        {
            string constr = "server=.;database=User1;uid=sa;pwd=123456";
            SqlConnection conn = new SqlConnection(constr);
            string sql = "select * from commodity";
            SqlCommand cmd = new SqlCommand(sql, conn);
            conn.Open();
            var read = cmd.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t{2}", "Id", "商品名称", "商品价格");
            while (read.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", read["commodityId"], read["commodityName"], read["price"]);
            }
            conn.Close();
        }

        public static void  PrintUserInfoAdaper()
        {
            SqlDataAdapter sqldataAdapter = new SqlDataAdapter(sql,constr);
            DataTable dataTable = new DataTable();
            sqldataAdapter.Fill(dataTable);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("{0}\t{1}\t{2}", "Id", "商品名称", "商品价格");
           for(int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}",dataTable.Rows[i]["commodityId"],dataTable.Rows[i]["commodityName"],dataTable.Rows[i]["price"]);
            }



        }

    }
}
