﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql_c
{
    class DBHelper
    {
        public SqlConnection Adress()
        {
            string adress = "server=.;database=demo;uid=sa;pwd=1234567879";
            SqlConnection sconn = new SqlConnection(adress);
            return sconn;
        }

        public int ExecNonQuery(string scmd)
        {
            Adress().Open();
            SqlCommand scomm = new SqlCommand(scmd, Adress());
            int i = scomm.ExecuteNonQuery();
            Adress().Close();
            return i;
        }

        public DataTable ExecQuery(string scmd)
        {
            DataTable dt = new DataTable();
            Adress().Open();
            SqlDataAdapter sda = new SqlDataAdapter(scmd,Adress());
            sda.Fill(dt);
            Adress().Close();
            return dt;
        }

    }
}
