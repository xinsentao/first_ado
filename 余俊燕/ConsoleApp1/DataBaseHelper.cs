﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DataBaseHelper
    {
        private readonly string sqlCommandString = "select * from UserName";
        private readonly string connectionString = "server =.;database = Admin3200;uid = sa;pwd = 123456";

        public static void PrintUserInfo()
        {
            string connectionString = "server =.;database = Admin3200;uid = sa;pwd = 123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from UserName";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "Id", "用户名", "年龄", "性别");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["Id"], sdr["Name"], sdr["Age"], sdr["Sex"]);
            }

            sqlConnection.Close();
        }


        public void PrintUserInfoAdaper() 
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommandString, connectionString);

            DataTable dataTable = new DataTable();

            dataAdapter.Fill(dataTable);

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "Id", "用户名", "年龄", "性别");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", dataTable.Rows[i]["Id"], dataTable.Rows[i]["Name"], dataTable.Rows[i]["Age"], dataTable.Rows[i]["Sex"]);
            }
        }
    }
}
